# Glasklar QA ST signing root

## How to generate a signing cert and key

The following command generates cert.pem and key.pem in the current
directory valid until the end of 2024:

    stmgr keygen certificate \
        -rootCert "Glasklar QA ST signing root cert.pem" \
        -rootKey  "priv/Glasklar QA ST signing root key.pem" \
        -validUntil 2024-12-31T23:59:59Z

The output is written to {cert,key}.pem.

## How the CA was set up

Soft key, 10y validity, Glasklar admins have access.

Key and cert generated 2024-01-29 on Linus' laptop.

    stmgr keygen certificate \
        -isCA \
        -certOut "Glasklar QA ST signing root cert.pem" \
        -keyOut  "Glasklar QA ST signing root key.pem" \
        -validUntil 2034-01-30T00:00:00Z

Key and cert added to Sigsum passdb.

    cat "Glasklar QA ST signing root key.pem"  | pass-sigsum add -m infra/glasklar-qa-st/key
    cat "Glasklar QA ST signing root cert.pem" | pass-sigsum add -m infra/glasklar-qa-st/cert
    pass-sigsum git push
